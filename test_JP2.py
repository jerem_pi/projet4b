from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import ChromeOptions
from selenium.webdriver.chrome.options import Options

System.setProperty("webdriver.chrome.driver", "C:\\Users\\jepicard\\S20\\chromedriver.exe");
ChromeOptions options=new ChromeOptions();
options.addArguments("start-maximized");
options.addArguments("disable-infobars");
options.addArguments("--disable-extensions");
options.addArguments("--disable-gpu");
options.addArguments("--disable-dev-shm-usage");
options.addArguments("--no-sandbox");
WebDriver driver = new ChromeDriver(options);
driver.get("https://google.com");
browser = webdriver.Chrome()

browser.get('http://www.univ-orleans.fr')


elem = browser.find_element_by_id('edit-search-api-fulltext')
elem.send_keys('iut orleans' + Keys.RETURN)
liens = browser.find_element_by_class_name('content').find_elements_by_tag_name('a')

# Vérifions qu'il y a au moins 3 liens
# sur cette recherche
assert(len(liens)>=3)
# Puis affichons ces liens
for l in liens:
    print(l.get_attribute('href'))

time.sleep(5)
browser.quit()
